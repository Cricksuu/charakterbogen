<?php
//Set configuration
switch($_SERVER["ENV"]) {
	case "jheise":
		require_once("D:/xampp/htdocs/hero/lib/internal/core.inc.php");
		break;
	case "work":
		require_once("C:/xampp/htdocs/hero/lib/internal/core.inc.php");
		break;
	case "tollstudios":
		require_once("/var/www/hero/lib/internal/core.inc.php");
		break;
	default:
		die(__FILE__.":".__LINE__.":".ERRORLIST['en']['F001']); //Errorcode F001
		break;
}
//URL redirect
if(!isset($_SERVER["REDIRECT_URL"])) {
	$_SERVER["REDIRECT_URL"] = BASEHTTP;
}

//URL splitting
$target = $_SERVER["REDIRECT_URL"];
$target=str_replace(BASEHTTP,"",$target);
if(substr($target, 0, 1) == '/') {
	$target=substr($target, 1);
}
if(substr($target, 0, -1) == '/') {
	$target=substr($target, 0, -1);
}
$target = explode('/', $target);
if($target) {
	foreach($target as &$targetRow) {
		$targetRow = urldecode($targetRow);
		unset($targetRow);
	}
}

//Assign splitted URL to smarty
$smarty->assign("target", $target);

//Load requested page
switch($target[0]) {
	case "":
	default:
		if($db->getOne('SELECT `id` FROM `character` WHERE `id` = '.(int)$target[0])) {
			require(LIBDIR."internal/pages/show.php");
		} else {
			require(LIBDIR."internal/pages/create.php");
		}
		die();
		break;
    case "create":
		require(LIBDIR."internal/pages/create.php");
		die();
		break;
}
