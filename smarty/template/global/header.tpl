<!doctype html>
<html lang="en">
    <head>
        <title>{$meta.title}</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="shortcut icon" href="{$meta.favicon}" type="image/x-icon">
        <link rel="icon" href="{$meta.favicon}" type="image/x-icon">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/open-iconic/1.1.1/font/css/open-iconic-bootstrap.css" />
        <link rel="stylesheet" href="{$BASEURL}/css/froala_blocks.css" type="text/css" />
        <style>
            form {
                margin-top: 20px;
            }
            .form-row {
                margin-bottom: 20px;
            }
            .user_input {
                width: 50px;
                height: auto;
                display: inline-block;
                border-bottom: solid 1px black;
            }
            .d-flex {
                padding: 0;
            }

			.oi-margin {
				margin-right: 10px;
				font-size: .8em;
			}
            @media print {
                .pagebreak {
                    page-break-after: always;
                }
            }
            footer {
                margin-top: 20px;
            }
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light d-print-none">
            <div class="container">
                <a class="navbar-brand" href="{$BASEURL}">{$brand.short}</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbar">
                    <div class="navbar-nav">
                        <a class="nav-item nav-link" href="{$BASEURL}/create"><span class="oi oi-margin oi-brush"></span>Charakter erstellen</a>
                        <a class="nav-item nav-link" href="http://howtobeahero.de"><span class="oi oi-margin oi-external-link"></span>How To Be A Hero Regelwerk</a>
                        {*TODO: Suchfunktion nach Name / ID*}
                    </div>
                </div>
            </div>
        </nav>
