<footer class="fdb-block footer-small bg-dark d-print-none">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-12 col-md-8">
            <div class="nav justify-content-center justify-content-md-start">
                <a class="nav-item nav-link" href="{$BASEURL}/create"><span class="oi oi-margin oi-brush"></span>Charakter erstellen</a>
                <a class="nav-item nav-link" href="http://howtobeahero.de"><span class="oi oi-margin oi-external-link"></span>How To Be A Hero Regelwerk</a>
                {*TODO: Suchfunktion nach Name / ID*}
            </div>
        </div>

        <div class="col-12 col-md-4 mt-4 mt-md-0 text-center text-md-right">
          <p>Made with <span class="heart">&#10084;</span> by Jan 'Cricksu' Heise</p>
        </div>
      </div>
    </div>
  </footer>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
        {literal}
        <script>
            $(document).ready(function() {
                var add_handeln = $("#add_handeln");
                var add_interagieren = $("#add_interagieren");
                var add_wissen = $("#add_wissen");

                var handeln_wrapper = $(".handeln_wrapper");
                var interagieren_wrapper = $(".interagieren_wrapper");
                var wissen_wrapper = $(".wissen_wrapper");

                $(add_handeln).click(function(e){
                    e.preventDefault();
                    $(handeln_wrapper).append(
                        "<div class='form-row'>" +
                            "<div class='col-8'>" +
                                "<input type='text' id='create_handeln_name[]' name='create_handeln_name[]' class='form-control' placeholder='Fähigkeit'>" +
                            "</div>" +
                            "<div class='col'>" +
                                "<input type='number' id='create_handeln_value[]' name='create_handeln_value[]' class='form-control' placeholder='Wert'>" +
                            "</div>" +
                        "</div>"
                    )
                });

                $(add_interagieren).click(function(e){
                    e.preventDefault();
                    $(interagieren_wrapper).append(
                        "<div class='form-row'>" +
                            "<div class='col-8'>" +
                                "<input type='text' id='create_interagieren_name[]' name='create_interagieren_name[]' class='form-control' placeholder='Fähigkeit'>" +
                            "</div>" +
                            "<div class='col'>" +
                                "<input type='number' id='create_interagieren_value[]' name='create_interagieren_value[]' class='form-control' placeholder='Wert'>" +
                            "</div>" +
                        "</div>"
                    )
                });

                $(add_wissen).click(function(e){
                    e.preventDefault();
                    $(wissen_wrapper).append(
                        "<div class='form-row'>" +
                            "<div class='col-8'>" +
                                "<input type='text' id='create_wissen_name[]' name='create_wissen_name[]' class='form-control' placeholder='Fähigkeit'>" +
                            "</div>" +
                            "<div class='col'>" +
                                "<input type='number' id='create_wissen_value[]' name='create_wissen_value[]' class='form-control' placeholder='Wert'>" +
                            "</div>" +
                        "</div>"
                    )
                });
            });
        </script>
        {/literal}
    </body>
</html>
