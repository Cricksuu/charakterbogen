{include file='global/header.tpl'}
<div class="container">
    <div class="row" style="margin: 40px 0px;">
        <div class="col-8 d-flex align-items-center">
            <div class="form-group">
                <label for="charName">Name</label>
                <h5 id="charName">{$char.meta.name}</h5>
            </div>
        </div>
        <div class="col d-flex align-items-center justify-content-end">
            <div class="form-group">
                <label for="charLP">Lebenspunkte</label>
                <h5 id="charLP"><span class="user_input"></span> von {$char.meta.lp}</h5>
            </div>
        </div>
    </div>
    <div class="row pagebreak">
        <div class="col-md-8">
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="charSex">Geschlecht</label>
                        <h5 id="charSex">{$char.meta.sex}</h5>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="charAge">Alter</label>
                        <h5 id="charAge">{$char.meta.age}</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="charBuild">Statur</label>
                        <h5 id="charBuild">{$char.meta.build}</h5>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="charReligion">Religion</label>
                        <h5 id="charReligion">{$char.meta.religion}</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="charProfession">Beruf</label>
                        <h5 id="charProfession">{$char.meta.profession}</h5>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="charMaritalStatus">Familienstand</label>
                        <h5 id="charMaritalStatus">{$char.meta.maritalStatus}</h5>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="form-group">
                        <label for="charOrigin">Herkunft</label>
                        <h5 id="charOrigin">{$char.meta.origin}</h5>
                    </div>
                </div>
                <div class="col">
                    <div class="form-group">
                        <label for="charMotherTongue">Muttersptache</label>
                        <h5 id="charMotherTongue">{$char.meta.motherTongue}</h5>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <img src="{$BASEURL}/img/chars/{if $char.meta.avatar == TRUE}{$target.0}{else}default{/if}.jpg" style="width: 100%; max-width: 400px; border: 1px solid #CCC; height: auto; object-fit: cover;"/>
        </div>
    </div>
    <div class="row">
        <h1>Fähigkeiten</h1>
    </div>
    <div class="row">
        <h3>Handeln</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="col-md-11">Fähigkeit</th>
                    <th class="col-md-1">Wert</th>
                </tr>
            </thead>
            <tbody>
                {foreach $char.skill.handeln as $skill}
                    <tr>
                        <td class="col-md-11">{$skill.name}</td>
                        <td class="col-md-1">{$skill.value}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
        <h3>Interagieren</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="col-md-11">Fähigkeit</th>
                    <th class="col-md-1">Wert</th>
                </tr>
            </thead>
            <tbody>
                {foreach $char.skill.interagieren as $skill}
                    <tr>
                        <td class="col-md-11">{$skill.name}</td>
                        <td class="col-md-1">{$skill.value}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
        <h3>Wissen</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th class="col-md-11">Fähigkeit</th>
                    <th class="col-md-1">Wert</th>
                </tr>
            </thead>
            <tbody>
                {foreach $char.skill.wissen as $skill}
                    <tr>
                        <td class="col-md-11">{$skill.name}</td>
                        <td class="col-md-1">{$skill.value}</td>
                    </tr>
                {/foreach}
            </tbody>
        </table>
        <a href="#" class="btn btn-block btn-primary d-print-none" onclick="window.print();return false;">Charakter drucken</a>
    </div>
</div>
{include file='global/footer.tpl'}
