{include file='global/header.tpl'}
<div class="container">
    <form method="POST">
        <h1>Charakter</h1>
        <div class="form-row">
            <div class="col-8">
                <input type="text" id="create_name" name="create_name" class="form-control" placeholder="Name">
            </div>
            <div class="col">
                <input type="number" id="create_lp" name="create_lp" class="form-control" placeholder="Leben">
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <input type="text" id="create_sex" name="create_sex" class="form-control" placeholder="Geschlecht">
            </div>
            <div class="col">
                <input type="number" id="create_age" name="create_age" class="form-control" placeholder="Alter">
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <input type="text" id="create_build" name="create_build" class="form-control" placeholder="Statur">
            </div>
            <div class="col">
                <input type="text" id="create_religion" name="create_religion" class="form-control" placeholder="Religion">
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <input type="text" id="create_profession" name="create_profession" class="form-control" placeholder="Beruf">
            </div>
            <div class="col">
                <input type="text" id="create_maritalStatus" name="create_maritalStatus" class="form-control" placeholder="Familienstand">
            </div>
        </div>
        <div class="form-row">
            <div class="col">
                <input type="text" id="create_origin" name="create_origin" class="form-control" placeholder="Herkunft">
            </div>
            <div class="col">
                <input type="text" id="create_motherTongue" name="create_motherTongue" class="form-control" placeholder="Muttersprache">
            </div>
        </div>
        {*
        <div class="form-row">
            <div class="form-group">
                <label for="create_avatar">Avatar</label>
                <input type="file" class="form-control-file" name="create_avatar" id="create_avatar">
            </div>
        </div>
        *}
        <h1>Fähigkeiten</h1>
		<h5>Jeder Charakter hat 500 Fähigkeiten-Punkte (FP) zum verteilen. Diese darf man frei auf die Fähigkeiten verteilen.</h5>
		<br />
        <h3>Handeln</h3>
		<p class="small">Beispiele: Rennen, Schlagen, Treten, Schießen, Schneidern, Kochen, Heben, TIschlern, Fischen, Verstecken</p>
        <div class="handeln_wrapper">
            <div class="form-row">
                <div class="col-8">
                    <input type="text" id="create_handeln_name[]" name="create_handeln_name[]" class="form-control" placeholder="Fähigkeit">
                </div>
                <div class="col">
                    <input type="number" id="create_handeln_value[]" name="create_handeln_value[]" class="form-control" placeholder="Wert">
                </div>
            </div>
        </div>
        <div class="form-row">
            <button type="button" id="add_handeln" class="btn btn-sm btn-primary"><span class="oi oi-margin oi-plus"></span>Fähigkeit hinzufügen</button>
        </div>
		<hr />
        <h3>Interagieren</h3>
		<p class="small">Beispiele: Feilschen, Lügen, Menschenkenntnis, Manipulieren, Betören, Beeindrucken, Unterhalten, Einschüchtern, Begeistern, Beruhigen</p>
        <div class="interagieren_wrapper">
            <div class="form-row">
                <div class="col-8">
                    <input type="text" id="create_interagieren_name[]" name="create_interagieren_name[]" class="form-control" placeholder="Fähigkeit">
                </div>
                <div class="col">
                    <input type="number" id="create_interagieren_value[]" name="create_interagieren_value[]" class="form-control" placeholder="Wert">
                </div>
            </div>
        </div>
        <div class="form-row">
            <button type="button" id="add_interagieren" class="btn btn-sm btn-primary"><span class="oi oi-margin oi-plus"></span>Fähigkeit hinzufügen</button>
        </div>
		<hr />
        <h3>Wissen</h3>
		<p class="small">Beispiele: Deutsch, Polnisch, Modedesign, Politik, Lesen/Schreiben, Mathematik, Literatur, Fauna, Flora, Latein</p>
        <div class="wissen_wrapper">
            <div class="form-row">
                <div class="col-8">
                    <input type="text" id="create_wissen_name[]" name="create_wissen_name[]" class="form-control" placeholder="Fähigkeit">
                </div>
                <div class="col">
                    <input type="number" id="create_wissen_value[]" name="create_wissen_value[]" class="form-control" placeholder="Wert">
                </div>
            </div>
        </div>
        <div class="form-row">
            <button type="button" id="add_wissen" class="btn btn-sm btn-primary"><span class="oi oi-margin oi-plus"></span>Fähigkeit hinzufügen</button>
        </div>
        <div class="form-row">
            <button type="submit" class="btn btn-block btn-success">Erstellung abschließen</button>
        </div>
    </form>
</div>
{include file='global/footer.tpl'}
