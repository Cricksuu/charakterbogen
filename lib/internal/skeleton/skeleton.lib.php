<?php
class skeleton {
	public $db;	// ADODB object
	protected $autoSave;

	// values
	private $currentData;	// Assoc Array
	private $untouchedData;	// Assoc Array
	private $checksum;
	// meta

	function __construct($autoSave=TRUE) {
		$this->_init($autoSave);
	}
	function __destruct() {
		$this->_final();
	}
	protected function _init($autoSave=TRUE) {
		if ( !is_object($this->db) ) {
			if ( !isset($GLOBALS['db']) ) {
				die(__FILE__.': '.__LINE__.': no database object found');
			}
			if ( !is_object($GLOBALS['db']) ) {
				die(__FILE__.': '.__LINE__.': database variable not an object');
			}
			if ( !method_exists($GLOBALS['db'],'GetRow') ) {
				die(__FILE__.': '.__LINE__.': unknown database object type');
			}
			$this->db = &$GLOBALS['db'];
		}
		$this->setAutoSave($autoSave);
		$this->_initValues();
	}
	protected function _final() {
		if ( $this->getAutoSave() && $this->isModified() ) {
			$this->save();
		}
	}
	protected function _initValues($array=FALSE) {
		$this->currentData = $array;
		$this->untouchedData = $array;
		$this->setChecksum();
	}
	protected function setValues($array) {
		if ( !is_array($array) ) {
			return FALSE;
		}
		foreach ( $array as &$row ) {
			if ( is_string($row) ) {
				$row = stripslashes($row);
			}
		}
		$this->currentData = $array;
		$this->untouchedData = $array;
		return TRUE;
	}
	public function load() {
		// stub
	}
	public function save() {
		// stub
	}
	public function set($key,$value) {
		if ( !isset($this->currentData[$key]) ) {
			return FALSE;
		}
		$this->currentData[$key] = $value;
	}
	public function get($key) {
		if ( !isset($this->currentData[$key]) ) {
			return FALSE;
		}
		return $this->currentData[$key];
	}
	public function getHistory($key) {
		if ( !isset($this->untouchedData[$key]) ) {
			return FALSE;
		}
		return $this->untouchedData[$key];
	}
	public function getAutoSave() {
		return (bool)$this->autoSave;
	}
	public function setAutoSave($autoSave) {
		$this->autoSave = (bool)$autoSave;
	}
	protected function getChecksum() {
		if ( !$this->currentData ) {
			return FALSE;
		}
		$checksum='';
		foreach ( $this->currentData as $key => $data ) {
			$checksum.='.'.(string)$key.'_'.$data;
		}
		return md5($checksum);
	}
	protected function setChecksum() {
		$this->checksum = $this->getChecksum();
	}
	public function isModified() {
		if ( $this->getChecksum() != $this->checksum ) {
			return TRUE;
		}
		return FALSE;
	}
}
