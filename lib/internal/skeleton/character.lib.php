<?php
class Character extends skeleton {
	function __construct($id = 0, $autoSave = TRUE) {
		parent::__construct($autoSave);
		$this->_initValuesEvent();
		if($id) {
			$this->set('id', $id);
			$this->load();
		}
	}
	function __destruct() {
		$this->_final();
	}
	protected function _final() {
		if($this->getAutoSave() && $this->isModified()) {
			$this->save();
		}
	}
	protected function _initValuesEvent() {
		$currentData['id'] = 0;
		$currentData['name'] = '';
        $currentData['lp'] = 0;
        $currentData['sex'] = '';
        $currentData['age'] = 0;
        $currentData['build'] = '';
        $currentData['religion'] = '';
        $currentData['profession'] = '';
        $currentData['maritalStatus'] = '';
        $currentData['origin'] = '';
        $currentData['motherTongue'] = '';
		parent::_initValues($currentData);
	}
	public function load() {
		$sql = 'SELECT * FROM `character` WHERE `id`='.(int)$this->get('id');
		$values = $this->db->GetRow($sql);
		if ( $this->db->ErrorNo() ) {
			error_log(__FILE__.': '.__LINE__.': MySQL error #'.$this->db->ErrorNo().': '.$this->db->ErrorMsg(),0);
			error_log("root cause (SQL query):\n".$sql);
			die();
		}
		if ( $values ) {
			$this->setValues($values);
		}
		$this->setChecksum();
	}
	public function save() {
		$sql = '';
		if($this->get('id')) {
			$sql .= 'UPDATE';
		} else {
			$sql .= 'INSERT INTO';
		}
		$sql .= '`character` SET
			`name` = '.$this->db->quote($this->get('name')).',
			`lp` = '.(int)$this->get('lp').',
            `sex` = '.$this->db->quote($this->get('sex')).',
            `age` = '.(int)$this->get('age').',
            `build` = '.$this->db->quote($this->get('build')).',
            `religion` = '.$this->db->quote($this->get('religion')).',
            `profession` = '.$this->db->quote($this->get('profession')).',
            `maritalStatus` = '.$this->db->quote($this->get('maritalStatus')).',
            `origin` = '.$this->db->quote($this->get('origin')).',
			`motherTongue`='.$this->db->quote($this->get('motherTongue'));
		if($this->get('id')) {
			$sql .= ' WHERE `id` = '.(int)$this->get('id');
		}
		$this->db->Execute($sql);
		if ( $this->db->ErrorNo() ) {
			error_log(__FILE__.': '.__LINE__.': MySQL error #'.$this->db->ErrorNo().': '.$this->db->ErrorMsg(),0);
			error_log("root cause (SQL query):\n".$sql);
			die();
		}
		$modified = $this->isModified();
		if ( $this->get('id') ) {
			$this->setChecksum();
		} else {
			$this->set('id',$this->db->Insert_ID());
		}
		if ( $modified ) {
			$this->saveHistory();
		}
		$this->load();
	}
	private function saveHistory() {

	}
}
