<?php
class Pointlist extends skeleton {
	function __construct($id = 0, $autoSave = TRUE) {
		parent::__construct($autoSave);
		$this->_initValuesEvent();
		if($id) {
			$this->set('id', $id);
			$this->load();
		}
	}
	function __destruct() {
		$this->_final();
	}
	protected function _final() {
		if($this->getAutoSave() && $this->isModified()) {
			$this->save();
		}
	}
	protected function _initValuesEvent() {
		$currentData['id'] = 0;
		$currentData['character'] = 0;
		$currentData['category'] = 0;
		$currentData['name'] = '';
		$currentData['value'] = 0;
		parent::_initValues($currentData);
	}
	public function load() {
		$sql = 'SELECT * FROM `pointlist` WHERE `id`='.(int)$this->get('id');
		$values = $this->db->GetRow($sql);
		if ( $this->db->ErrorNo() ) {
			error_log(__FILE__.': '.__LINE__.': MySQL error #'.$this->db->ErrorNo().': '.$this->db->ErrorMsg(),0);
			error_log("root cause (SQL query):\n".$sql);
			die();
		}
		if ( $values ) {
			$this->setValues($values);
		}
		$this->setChecksum();
	}
	public function save() {
		$sql = '';
		if($this->get('id')) {
			$sql .= 'UPDATE';
		} else {
			$sql .= 'INSERT INTO';
		}
		$sql .= '`pointlist` SET
			`character` = '.(int)$this->get('character').',
			`category` = '.(int)$this->get('category').',
			`name` = '.$this->db->quote($this->get('name')).',
			`value` = '.(int)$this->get('value');
		if($this->get('id')) {
			$sql .= ' WHERE `id` = '.(int)$this->get('id');
		}
		$this->db->Execute($sql);
		if ( $this->db->ErrorNo() ) {
			error_log(__FILE__.': '.__LINE__.': MySQL error #'.$this->db->ErrorNo().': '.$this->db->ErrorMsg(),0);
			error_log("root cause (SQL query):\n".$sql);
			die();
		}
		$modified = $this->isModified();
		if ( $this->get('id') ) {
			$this->setChecksum();
		} else {
			$this->set('id',$this->db->Insert_ID());
		}
		if ( $modified ) {
			$this->saveHistory();
		}
		$this->load();
	}
	private function saveHistory() {

	}
}
