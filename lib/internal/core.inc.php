<?php
date_default_timezone_set("Europe/Berlin");
require_once("errorList.inc.php");
switch($_SERVER["ENV"]) {
	case "jheise":
		define("BASEDIR", "D:/xampp/htdocs/hero/");
		define("BASEHTTP", "");
		define("BASEURL", "http://hero");
		break;
	case "work":
		define("BASEDIR", "C:/xampp/htdocs/hero/");
		define("BASEHTTP", "");
		define("BASEURL", "http://hero.localhost");
		break;
	case "tollstudios":
		define("BASEDIR", "/var/www/hero/");
		define("BASEHTTP", "");
		define("BASEURL", "http://hero.tollstudios.com");
		break;
	default:
		die(__FILE__.":".__LINE__.":".ERRORLIST['en']['F001']); //Errorcode F001
		break;
}


define("DEBUG", TRUE);
define("TMPDIR", BASEDIR."tmp/");
define("LIBDIR", BASEDIR."lib/");
define("COOKIEDIR", TMPDIR."cookie/");

if(DEBUG == TRUE) {
	ini_set("display_errors", 1);
	ini_set("html_errors", 1);
} else {
	ini_set("display_errors", 0);
	ini_set("html_errors", 0);
}

require_once(LIBDIR."internal/core.lib.php");
require_once(LIBDIR."internal/db.inc.php");
require_once(LIBDIR."internal/smarty.inc.php");
require_once(LIBDIR."internal/skeleton/skeleton.lib.php");

$brand = array(
	'long' => 'HTBAH Charakter Erstellung',
	'short' => 'HTBAH',
	'slogan' => 'Create your hero!',
);

$smarty->assign('brand', $brand);
