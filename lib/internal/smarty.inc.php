<?php
require_once(LIBDIR.'/smarty/libs/Smarty.class.php');

$smarty = new Smarty();

$smarty->setTemplateDir(BASEDIR.'smarty/template');
$smarty->setCompileDir(TMPDIR.'smarty/compile');
$smarty->setConfigDir(LIBDIR.'internal/smarty/config');
$smarty->setCacheDir(TMPDIR.'smarty/cache');
$smarty->addPluginsDir(LIBDIR.'internal/smarty/plugin');

$smarty->assign('BASEHTTP',BASEHTTP);
$smarty->assign('BASEURL',BASEURL);
