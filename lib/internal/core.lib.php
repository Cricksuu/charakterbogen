<?php
function p($var = "") {
	if ( php_sapi_name() != 'cli' ) echo '<pre>';
	var_dump($var);
	if ( php_sapi_name() != 'cli' ) echo '</pre>';
}

function loadUser($id) {
	global $db;
	if(!is_array($id)) {
		$id = array($id);
	}
	$user = array();
	foreach($id as $currentId) {
		$user[] = $db->getAll(
			'SELECT
			`user`.`id` as `id`,
			`user`.`username` as `username`,
			`user`.`displayname` as `displayname`,
			`user`.`email` as `email`,
			`user`.`website` as `website`,
			`user`.`location` as `location`,
			`user`.`organization` as `organization`,
			`user`.`dateCreated` as `dateCreated`,
			`user`.`active` as `active`,
			`user`.`locked` as `locked`,
			`user`.`public` as `public`
			FROM `user` WHERE `user`.`id` = '.(int)$currentId);

	}
	return $user;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateActivationKey($id) {
	$key = generateRandomString(5).$id;
	return $key;
}
