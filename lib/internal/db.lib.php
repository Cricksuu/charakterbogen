<?php
function sessionQueryCacheGet($sql,$mode) {
	global $sessionQuerySelectCache;
	if ( !isset($sessionQuerySelectCache) ) {
		$sessionQuerySelectCache = array();
	}
	if ( !isset($sessionQuerySelectCache[md5($sql)]) ) {
		switch ( $mode ) {
			case 'one':
				$sessionQuerySelectCache[md5($sql)] = $GLOBALS['dbCore']->getOne($sql);
				break;
			case 'row':
				$sessionQuerySelectCache[md5($sql)] = $GLOBALS['dbCore']->getRow($sql);
				break;
			case 'all':
				$sessionQuerySelectCache[md5($sql)] = $GLOBALS['dbCore']->getAll($sql);
				break;
			default:
				die(__FILE__.': '.__LINE__.': unknown mode "'.$mode."\"\n");
				break;
		}
	}
	return $sessionQuerySelectCache[md5($sql)];
}
