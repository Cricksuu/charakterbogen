<?php
define("ERRORLIST", array(
	"de" => array(
		"F001" => "FATAL: Umgebung konnte nicht gefunden werden (Fehlercode F001).",
		"F002" => "FATAL: Es konnte keine Verbindung zur Datenbank hergestellt werden (Fehlercode F002).",
		"F003" => "Das Passwort stimmt nicht mit der Bestätigung überein.",
		"F004" => "Es existiert bereits ein Account mit diesem Benutzernamen oder dieser Email Adresse.",
		"F005" => "Ungültige Benutzerdaten",
	),
	"en" => array(
		"F001" => "FATAL: Could not find environment (Errorcode F001).",
		"F002" => "FATAL: Could not connect to the database (Errorcode F002).",
		"F003" => "You have entered an invalid password.",
		"F004" => "This account already exists.",
		"F005" => "Invalid username or password",
	),
));
// echo "<pre>";
// var_dump($errorList);
// echo "</pre>";
