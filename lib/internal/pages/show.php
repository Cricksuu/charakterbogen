<?php
require(LIBDIR.'internal/skeleton/character.lib.php');
require(LIBDIR.'internal/skeleton/pointList.lib.php');

if(is_file(BASEDIR."pub/img/chars/".$target[0].".jpg")) {
    $avatar = TRUE;
} else {
    $avatar = FALSE;
}

$char['meta'] = $db->getRow('SELECT * FROM `character` WHERE `id` = '.(int)$target[0]);
$char['skill']['handeln'] = $db->getAll('SELECT `name`, `value` FROM `pointlist` WHERE `category` = 1 AND `character` = '.(int)$target[0]);
$char['skill']['interagieren'] = $db->getAll('SELECT `name`, `value` FROM `pointlist` WHERE `category` = 2 AND `character` = '.(int)$target[0]);
$char['skill']['wissen'] = $db->getAll('SELECT `name`, `value` FROM `pointlist` WHERE `category` = 3 AND `character` = '.(int)$target[0]);
$char['meta']['avatar'] = $avatar;

$meta = array(
    'title' => $char['meta']['name'].' - How To Be A Hero',
    'favicon' => '',
);

$smarty->assign('meta', $meta);
$smarty->assign('char', $char);
$smarty->display('show.tpl');
