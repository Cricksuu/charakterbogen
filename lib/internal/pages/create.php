<?php
require(LIBDIR.'internal/skeleton/character.lib.php');
require(LIBDIR.'internal/skeleton/pointList.lib.php');

if(isset($_POST['create_name']) &&
isset($_POST['create_lp']) &&
isset($_POST['create_sex']) &&
isset($_POST['create_age']) &&
isset($_POST['create_build']) &&
isset($_POST['create_religion']) &&
isset($_POST['create_profession']) &&
isset($_POST['create_maritalStatus']) &&
isset($_POST['create_origin']) &&
isset($_POST['create_motherTongue']) &&
isset($_POST['create_handeln_name']) &&
isset($_POST['create_handeln_value']) &&
isset($_POST['create_interagieren_name']) &&
isset($_POST['create_interagieren_value']) &&
isset($_POST['create_wissen_name']) &&
isset($_POST['create_wissen_value'])) {
    $newChar = new Character();
    $newChar->set('name', $_POST['create_name']);
    $newChar->set('lp', $_POST['create_lp']);
    $newChar->set('sex', $_POST['create_sex']);
    $newChar->set('age', $_POST['create_age']);
    $newChar->set('build', $_POST['create_build']);
    $newChar->set('religion', $_POST['create_religion']);
    $newChar->set('profession', $_POST['create_profession']);
    $newChar->set('maritalStatus', $_POST['create_maritalStatus']);
    $newChar->set('origin', $_POST['create_origin']);
    $newChar->set('motherTongue', $_POST['create_motherTongue']);
    $newChar->save();

    for($i = 0; $i < sizeof($_POST['create_handeln_name']); $i++) {
        if($_POST['create_handeln_name'][$i] != "" && $_POST['create_handeln_value'][$i] != "") {
            $newPoint = new PointList();
            $newPoint->set('character', $newChar->get('id'));
            $newPoint->set('category', 1);
            $newPoint->set('name', $_POST['create_handeln_name'][$i]);
            $newPoint->set('value', $_POST['create_handeln_value'][$i]);
            $newPoint->save();
            unset($newPoint);
        }
    }

    for($j = 0; $j < sizeof($_POST['create_interagieren_name']); $j++) {
        if($_POST['create_interagieren_name'][$j] != "" && $_POST['create_interagieren_value'][$j] != "") {
            $newPoint = new PointList();
            $newPoint->set('character', $newChar->get('id'));
            $newPoint->set('category', 2);
            $newPoint->set('name', $_POST['create_interagieren_name'][$j]);
            $newPoint->set('value', $_POST['create_interagieren_value'][$j]);
            $newPoint->save();
            unset($newPoint);
        }
    }

    for($k = 0; $k < sizeof($_POST['create_wissen_name']); $k++) {
        if($_POST['create_wissen_name'][$k] != "" && $_POST['create_wissen_value'][$k] != "") {
            $newPoint = new PointList();
            $newPoint->set('character', $newChar->get('id'));
            $newPoint->set('category', 3);
            $newPoint->set('name', $_POST['create_wissen_name'][$k]);
            $newPoint->set('value', $_POST['create_wissen_value'][$k]);
            $newPoint->save();
            unset($newPoint);
        }
    }
    if(isset($_FILES['create_avatar'])) {
        $userDir = BASEDIR."pub/img/chars";
        if(!is_dir($userDir)) {
            mkdir($userDir);
        }
        move_uploaded_file($_FILES["create_avatar"]["tmp_name"], $userDir."/".$newChar->get('id').".jpg");
    }
    header('Location: '.BASEURL.'/'.$newChar->get('id'));
    unset($newChar, $i, $j, $k);
}

$meta = array(
    'title' => 'Create your hero! How To Be A Hero',
    'favicon' => '',
);

$smarty->assign('meta', $meta);
$smarty->display('create.tpl');
